#
# Copyright (C) 2018 Guido Berhoerster <guido+xwrited@berhoerster.name>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

PACKAGE =	xwrited
APPLICATION_ID = org.guido-berhoerster.code.xwrited
VERSION =	3
DISTNAME :=	$(PACKAGE)-$(VERSION)
BUG_ADDRESS =	guido+xwrited@berhoerster.name

# gcc, clang, icc, Sun/Solaris Studio
CC :=		$(CC) -std=c99
COMPILE.c =	$(CC) $(CFLAGS) $(XCFLAGS) $(CPPFLAGS) $(XCPPFLAGS) $(TARGET_ARCH) -c
# gcc, clang, icc
MAKEDEPEND.c =	$(CC) -MM $(CFLAGS) $(XCFLAGS) $(CPPFLAGS) $(XCPPFLAGS)
# Sun/Solaris Studio
#MAKEDEPEND.c =	$(CC) -xM1 $(CFLAGS) $(XCFLAGS) $(CPPFLAGS) $(XCPPFLAGS)
# X makedepend
#MAKEDEPEND.c =	makedepend -f- -Y -- $(CFLAGS) $(XCFLAGS) $(CPPFLAGS) $(XCPPFLAGS) --
LINK.c =	$(CC) $(CFLAGS) $(XCFLAGS) $(CPPFLAGS) $(XCPPFLAGS) $(LDFLAGS) $(XLDFLAGS) $(TARGET_ARCH)
LINK.o =	$(CC) $(LDFLAGS) $(XLDFLAGS) $(TARGET_ARCH)
CP :=		cp
INSTALL :=	install
INSTALL.exec :=	$(INSTALL) -D -m 0755
INSTALL.data :=	$(INSTALL) -D -m 0644
INSTALL.link :=	$(CP) -f -P
PAX :=		pax
GZIP :=		gzip
SED :=		sed
XSLTPROC :=	xsltproc
DOCBOOK5_MANPAGES_STYLESHEET =	http://docbook.sourceforge.net/release/xsl-ns/current/manpages/docbook.xsl
DOCBOOK5_MANPAGES_FLAGS = --stringparam man.authors.section.enabled 0 \
		--stringparam man.copyright.section.enabled 0
MSGFMT =	msgfmt
MSGMERGE =	msgmerge
XGETTEXT =	xgettext
XGETTEXT_OPTIONS = --copyright-holder "$(AUTHOR)" \
		--package-name '$(PACKAGE)' \
		--package-version '$(VERSION)' \
		--msgid-bugs-address '$(BUG_ADDRESS)' \
		--default-domain '$(PACKAGE)' \
		--from-code UTF-8 \
		--keyword=_ \
		--keyword=N_ \
		--keyword=C_:1c,2 \
		--keyword=NC_:1c,2 \
		--keyword=g_dngettext:2,3 \
		--add-comments

define generate-manpage-rule =
%.$(1): %.$(1).xml
	$$(XSLTPROC) \
	    --xinclude \
	    --stringparam package $$(PACKAGE) \
	    --stringparam version $$(VERSION)\
	    docbook-update-source-data.xsl $$< | \
	    $$(XSLTPROC) \
	    --xinclude \
	    $$(DOCBOOK5_MANPAGES_FLAGS) \
	    --output $$@ \
	    $$(DOCBOOK5_MANPAGES_STYLESHEET) \
	    -
endef

DESTDIR ?=
prefix ?=	/usr/local
bindir ?=	$(prefix)/bin
datadir ?=	$(prefix)/share
mandir ?=	$(datadir)/man
localedir ?=	$(datadir)/locale
sysconfdir ?=	/etc
xdgautostartdir ?= $(sysconfdir)/xdg/autostart

OS_NAME :=	$(shell uname -s)
OS_RELEASE :=	$(shell uname -r)

OBJS =		xwrited.o \
		xwd-application.o
ifneq ($(findstring $(OS_NAME),Linux FreeBSD),)
  OBJS +=	xwd-utmp-utempter.o
else
  OBJS +=	xwd-utmp-utmpx.o
endif

MANPAGES =	$(PACKAGE).1

AUTOSTART_FILE = $(APPLICATION_ID).desktop

LINGUAS :=	$(shell sed 's/\#.*//' po/LINGUAS)
MOFILES :=	$(patsubst %,po/%.mo,$(LINGUAS))
POTFILES_IN :=	$(shell sed 's/\#.*//' po/POTFILES.in)
POTFILE =	po/$(PACKAGE).pot

.DEFAULT_TARGET = all

.PHONY: all pot update-po clean clobber dist install

all: $(PACKAGE) $(AUTOSTART_FILE) $(MOFILES) $(MANPAGES)

$(PACKAGE): XCFLAGS =	$(shell pkg-config --cflags gobject-2.0 gio-2.0 \
			glib-2.0) \
			-DPACKAGE=\"$(PACKAGE)\" \
			-DAPPLICATION_ID=\"$(APPLICATION_ID)\" \
			-DVERSION=\"$(VERSION)\" \
			-DG_LOG_DOMAIN=\"$(PACKAGE)\" \
			-DPACKAGE_LOCALE_DIR="\"$(localedir)\"" \
			-DGETTEXT_PACKAGE=\"$(PACKAGE)\"
$(PACKAGE): LDLIBS =	$(shell pkg-config --libs gobject-2.0 gio-2.0 \
			glib-2.0)

ifeq ($(OS_NAME),Linux)
  $(PACKAGE): XCPPFLAGS += -D_XOPEN_SOURCE=600
  $(PACKAGE): LDLIBS +=	-lutempter
else ifeq ($(OS_NAME),FreeBSD)
  $(PACKAGE): LDLIBS +=	-lulog
else ifeq ($(OS_NAME),SunOS)
  $(PACKAGE): XCPPFLAGS += -D_XOPEN_SOURCE=600 -I/usr/xpg4/include
  $(PACKAGE): XLDFLAGS += -L/usr/xpg4/lib -R/usr/xpg4/lib
endif

$(PACKAGE): $(OBJS)
	$(LINK.o) $^ $(LDLIBS) -o $@

$(POTFILE): po/POTFILES.in $(POTFILES_IN)
	$(XGETTEXT) $(XGETTEXT_OPTIONS) --files-from $< --output $@

pot: $(POTFILE)

update-po: $(POTFILE)
	for pofile in $(patsubst %.mo,%.po,$(MOFILES)); do \
	    $(MSGMERGE) --update --backup off $$pofile $<; \
	done

%.mo: %.po
	$(MSGFMT) --output $@ $<

%.desktop: %.desktop.in
	$(MSGFMT) --desktop --template $< -d po --output $@

$(foreach section,1 2 3 4 5 6 7 8 9,$(eval $(call generate-manpage-rule,$(section))))

%.mo: %.po
	$(MSGFMT) -o $@ $<

%.o: %.c
	$(MAKEDEPEND.c) $< | $(SED) -f deps.sed >$*.d
	$(COMPILE.c) -o $@ $<

install: all
	$(INSTALL.exec) $(PACKAGE) "$(DESTDIR)$(bindir)/$(PACKAGE)"
	$(INSTALL.data) $(AUTOSTART_FILE) \
	    $(DESTDIR)$(xdgautostartdir)/$(AUTOSTART_FILE)
	for lang in $(LINGUAS); do \
	   $(INSTALL.data) po/$${lang}.mo \
	        $(DESTDIR)$(localedir)/$${lang}/LC_MESSAGES/$(PACKAGE).mo; \
	done
	for manpage in $(MANPAGES); do \
	    $(INSTALL.data) $${manpage} \
	        "$(DESTDIR)$(mandir)/man$${manpage##*.}/$${manpage##*/}"; \
	done

clean:
	rm -f $(PACKAGE) $(OBJS) $(AUTOSTART_FILE) $(POTFILE) $(MOFILES) \
	    $(MANPAGES)

clobber: clean
	rm -f $(patsubst %.o,%.d,$(OBJS))

dist: clobber
	$(PAX) -w -x ustar -s ',.*/\..*,,' -s ',./[^/]*\.tar\.gz,,' \
	    -s ',^\.$$,,' -s ',\./,$(DISTNAME)/,' . | \
	    $(GZIP) > $(DISTNAME).tar.gz

-include local.mk

-include $(patsubst %.o,%.d,$(OBJS))
